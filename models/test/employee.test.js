const Employee = require('../employee.model');
const mongoose = require('mongoose');
const expect = require('chai').expect;

describe('Employee', () => {
  it('should throw an error if no "firstName", "lastName" or "department" arg', () => {
    const cases = [
      {lastName: 'Doe', department: 'IT'},
      {firstName: 'Amanda', department: 'IT'},
      {firstName: 'Amanda', lastName: 'Doe'}
    ];
    const missing = ['firstName', 'lastName', 'department'];

    for(let data in cases) {
      const dep = new Employee(cases[data]);

      dep.validate(err => {
        expect(err.errors[missing[data]]).to.exist;
      });
    }
  });

  it('should throw an error if no "firstName", "lastName" or "department" is not a string', () => {
    const cases = [
      {firstName: [], lastName: 'Doe', department: 'IT'},
      {firstName: {}, lastName: 'Doe', department: 'IT'},
      {firstName: 'Amanda', lastName: [], department: 'IT'},
      {firstName: 'Amanda', lastName: {}, department: 'IT'},
      {firstName: 'Amanda', lastName: 'Doe', department: []},
      {firstName: 'Amanda', lastName: 'Doe', department: {}}
    ];
    const missing = ['firstName', 'lastName', 'department'];
    for(let data in cases) {
      const dep = new Employee(cases[data]);

      dep.validate(err => {
        expect(err.errors[missing[Math.floor((parseInt(data)) / 2)]]).to.exist;
      });
    }
  });

  it('should not throw an error if "firstName", "lastName" and "department" is okay', () => {
    const cases = [
      {firstName: 'Amanda', lastName: 'Doe', department: 'IT'},
      {firstName: 'John', lastName: 'Doe', department: 'Marketing'},
      {firstName: 'Jane', lastName: 'Smith', department: 'Testing'}
    ];

    for(let data of cases) {
      const dep = new Employee(data);

      dep.validate(err => {
        expect(err).to.not.exist;
      });
    }
  });
});